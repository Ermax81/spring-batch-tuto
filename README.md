Projet exemple pour une utilisation simple de spring-batch avec le theme 'car' (ie voiture).

Source: https://github.com/TechPrimers/spring-batch-example-1

Vidéo: https://www.youtube.com/watch?v=1XEX-u12i0A

Les principaux Beans sont décrits dans la classe SpringBatchConfig.

Après le démarrage de l'application SpringBoot SpringBatchCarApplication, il est possible de lancer le batch de chargement du fichier cars.csv (répertoire resources) via l'URL 
[Start Job: http://localhost:8080/load](http://localhost:8080/load "Lancement job")

[Accès à la Console H2: http://localhost:8080/h2](http://localhost:8080/h2 "Connexion à la console H2")

    Driver Class: org.h2.Driver
    JDBC URL: jdbc:h2:mem:testdb
    User Name: sa
    Password: password

Voir le fichier h2_checks.sql (répertoire resources) pour visualiser les commandes SQL permettant de vérifier les infos d'exécution du batch.

Remarque / Point à travailler: il y a un problème de correspondance (typage String -> Number) lors de la constitution du Bean Car à partir du fichier csv. 

Piste d'évolution pour y répondre: travailler sur le LineMapper, et/ou le BeanWrapperFieldSetMapper.


**Historique versions:**

master  
- Renommage SpringBatchCar->SpringBatchTuto
- Ajout mapper spécifique pour l'Entity Car 

1.0.0   
- Version initiale simple pour comprendre le fonctionnement
