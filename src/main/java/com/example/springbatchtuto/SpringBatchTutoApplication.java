package com.example.springbatchtuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchTutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchTutoApplication.class, args);
	}

}
