package com.example.springbatchtuto.config;

import com.example.springbatchtuto.batch.DBWriter;
import com.example.springbatchtuto.batch.Processor;
import com.example.springbatchtuto.mapper.RecordFieldSetMapperForCar;
import com.example.springbatchtuto.model.Car;
import com.example.springbatchtuto.repository.CarRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    private CarRepository carRepository;

    @Bean
    public Job job(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory,
                   ItemReader<Car> itemReader,
                   ItemProcessor<Car,Car> itemProcessor,
                   ItemWriter<Car> itemWriter) {

        int chunkSize = 10;
        Step step = stepBuilderFactory.get("ETL-FileLoad") //table batch_step_execution
                .<Car,Car>chunk(chunkSize)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();

        return jobBuilderFactory.get("ETL-Load") //table batch_job_instance
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    @Bean
    public DBWriter itemWriter() {
        return new DBWriter(carRepository);
    }

    @Bean
    public Processor itemProcessor() {
        return new Processor();
    }

    @Bean
    public FlatFileItemReader<Car> itemReader() {
        FlatFileItemReader<Car> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new FileSystemResource("src/main/resources/cars.csv"));
        flatFileItemReader.setName("CSV-Reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(lineMapper());
        return flatFileItemReader;
    }

    public LineMapper<Car> lineMapper() {
        DefaultLineMapper<Car> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(";");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("marque", "denomination", "prix");
        defaultLineMapper.setLineTokenizer(lineTokenizer);

        //BeanWrapperFieldSetMapper<Car> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        //fieldSetMapper.setTargetType(Car.class); //Ne fonctionnera pas si il existe au moins une propriété différente de string
        //defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        defaultLineMapper.setFieldSetMapper(new RecordFieldSetMapperForCar());

        return defaultLineMapper;
    }


}
