package com.example.springbatchtuto.batch;

import com.example.springbatchtuto.model.Car;
import lombok.NoArgsConstructor;
import org.springframework.batch.item.ItemProcessor;

@NoArgsConstructor
public class Processor implements ItemProcessor<Car, Car> {

    @Override
    public Car process(Car car) throws Exception {
        System.out.println("Processing car " + car.toString());
        return car;
    }
}
