package com.example.springbatchtuto.batch;

import com.example.springbatchtuto.model.Car;
import com.example.springbatchtuto.repository.CarRepository;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class DBWriter implements ItemWriter<Car> {

    private CarRepository carRepository;

    public DBWriter(CarRepository repository) {
        this.carRepository = repository;
    }

    @Override
    public void write(List<? extends Car> list) throws Exception {
        this.carRepository.saveAll(list);
        System.out.println("Data saved for cars ");
    }
}
