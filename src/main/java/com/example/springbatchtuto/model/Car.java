package com.example.springbatchtuto.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@Setter
@Getter
@ToString
@NoArgsConstructor
public class Car {

    //Si modification, ne pas oublier de modifier RecordFieldSetMapperForCar

    @Id
    @GeneratedValue
    private Integer id;

    private String marque;

    private String denomination;

    private double prix;
}
