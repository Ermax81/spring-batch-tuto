package com.example.springbatchtuto.mapper;

import com.example.springbatchtuto.model.Car;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

//A modifier si modification Entity Car
public class RecordFieldSetMapperForCar implements FieldSetMapper<Car> {
    @Override
    public Car mapFieldSet(FieldSet fieldSet) throws BindException {
        Car car = new Car();
        car.setMarque(fieldSet.readString("marque")); //0
        car.setDenomination(fieldSet.readString("denomination")); //1
        car.setPrix(fieldSet.readDouble("prix")); //2
        return car;
    }
}
