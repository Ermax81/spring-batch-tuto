package com.example.springbatchtuto.repository;

import com.example.springbatchtuto.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {
    Optional<Car> findCarByMarqueAndDenomination( String marque, String denomination );
}
