package com.example.springbatchtuto.repository;

import com.example.springbatchtuto.model.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) //https://stackoverflow.com/questions/52551718/what-use-is-testinstance-annotation-in-junit-5
public class CarRepositoryTest {

    @Autowired
    private CarRepository magasin;

    @BeforeAll
    public void setup() {
        Car carSample = new Car();
        carSample.setMarque("Renault");
        carSample.setDenomination("Zoe 50");
        carSample.setPrix(29000);
        magasin.save(carSample);
    }

    @Test
    public void checkZoe() {
        Optional<Car> carDb = magasin.findCarByMarqueAndDenomination("Renault", "Zoe 50");
        Assertions.assertTrue( carDb.isPresent() );
    }
}
